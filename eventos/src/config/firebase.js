import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDGmfm3mAAJAgKwDaoF340KOR-aFhZK0HA",
  authDomain: "eventos-f8947.firebaseapp.com",
  databaseURL: "https://eventos-f8947.firebaseio.com",
  projectId: "eventos-f8947",
  storageBucket: "eventos-f8947.appspot.com",
  messagingSenderId: "1010275320388",
  appId: "1:1010275320388:web:8f4663e3caa234bc4bf211",
};

export default firebase.initializeApp(firebaseConfig);
