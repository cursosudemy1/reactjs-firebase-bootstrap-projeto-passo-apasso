import React, { useState } from "react";
import firebase from "../../config/firebase";
import { Link, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "firebase/auth";
import "./login.css";

function Login() {
  const [email, setEmail] = useState();
  const [senha, setSenha] = useState();
  const [msgTipo, setMsgTipo] = useState();

  const dispatch = useDispatch();

  function logar() {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, senha)
      .then((resultado) => {
        setMsgTipo("sucesso");
        // Aguarda um tempo para executar dispatch
        setTimeout(() => {
          dispatch({ type: "LOG_IN", usuarioEmail: email });
        }, 2000);
      })
      .catch((erro) => {
        setMsgTipo("erro");
      });
  }

  return (
    <div className="login-content d-flex align-items-center">
      {useSelector((state) => state.usuarioLogado) > 0 ? (
        <Redirect to="/" />
      ) : null}

      <form className="form-signin mx-auto">
        <div className="text-center mb-4">
          <i class="fas fa-user-friends text-warning fa-4x"></i>
          <h1 className="h3 mb-3 font-weight-bold text-white">Login</h1>
        </div>

        <input
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          id="inputEmail"
          className="form-control my-2"
          placeholder="Email"
        />

        <input
          onChange={(e) => setSenha(e.target.value)}
          type="password"
          id="inputPassword"
          className="form-control my-2"
          placeholder="Senha"
        />

        <button
          onClick={logar}
          className="btn btn-lg btn-login btn-block"
          type="button"
        >
          Logar
        </button>

        <div className="msg-login text-white text-center my-5">
          {msgTipo === "sucesso" && (
            <span>
              <strong>Wow!</strong> Você está conectado !
            </span>
          )}
          {msgTipo === "erro" && (
            <span>
              <strong>Ops!</strong> Verifique se a senha ou usuário estão
              corretos !
            </span>
          )}
        </div>

        <div className="opcoes-login mt-5 text-center">
          <Link to="usuariorecuperarsenha" className="mx-2">
            Recuperar Senha
          </Link>

          <Link to="novousuario" className="mx-2">
            Quero Cadastrar
          </Link>
        </div>
      </form>
    </div>
  );
}

export default Login;
